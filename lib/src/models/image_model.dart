class ImageModel {
  late int id;
  late String title;
  late String url;

  ImageModel({required this.id, required this.title, required this.url});

  ImageModel.fromJson(Map<String, dynamic> parsedJson) {
    id = parsedJson['id'];
    title = parsedJson['title'];
    url = parsedJson['url'];
  }
}
