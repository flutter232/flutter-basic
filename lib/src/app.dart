import 'package:flutter/material.dart';
import 'package:http/http.dart' show get;
import 'package:http/http.dart' show Response;
import 'package:learn_fllutter/src/models/image_model.dart';
import 'dart:convert' show json;

import 'package:learn_fllutter/src/widgets/image_list.dart';

class App extends StatefulWidget {
  const App({Key? key}) : super(key: key);
  @override
  State<StatefulWidget> createState() {
    return AppState();
  }
}

class AppState extends State<App> {
  int number = 0;
  List<ImageModel> images = [];
  void fetchImage() {
    number++;
    var url = Uri.https("jsonplaceholder.typicode.com", "photos/$number");
    get(url).then((Response response) {
      var image = ImageModel.fromJson(json.decode(response.body));
      setState(() {
        images.add(image);
        number++;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Contador de Pessoas",
      home: Scaffold(
        appBar: AppBar(
          title: const Text("Hello Flutter"),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: fetchImage,
          child: const Icon(Icons.add),
          backgroundColor: Colors.pink,
        ),
        body: ImageList(images: images),
      ),
    );
  }
}
